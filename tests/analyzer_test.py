import unittest

from game.analyzer import Analyzer
from tests.grid_factory import GridFactory


class TestAnalyzer(unittest.TestCase):
    def setUp(self):
        self.analyzer = Analyzer()

    def test_has_winner_should_return_true_if_winner_from_first_column_on_first_row(self):
        # given
        grid = GridFactory.with_winner_from_first_column_on_first_row()

        # when
        has_winner = self.analyzer.has_winner(grid)

        # then
        self.assertTrue(has_winner)

    def test_has_winner_should_return_true_if_winner_from_second_column_on_first_row(self):
        # given
        grid = GridFactory.with_winner_from_second_column_on_first_row()

        # when
        has_winner = self.analyzer.has_winner(grid)

        # then
        self.assertTrue(has_winner)

    def test_has_winner_should_return_true_if_winner_from_third_column_on_first_row(self):
        # given
        grid = GridFactory.with_winner_from_third_column_on_first_row()

        # when
        has_winner = self.analyzer.has_winner(grid)

        # then
        self.assertTrue(has_winner)

    def test_has_winner_should_return_true_if_winner_from_fourth_column_onnd_first_row(self):
        # given
        grid = GridFactory.with_winner_from_fourth_column_on_first_row()

        # when
        has_winner = self.analyzer.has_winner(grid)

        # then
        self.assertTrue(has_winner)

    def test_has_winner_should_return_false_if_no_winner_on_first_row(self):
        # given
        grid = GridFactory.without_winner_on_first_row()

        # when
        has_winner = self.analyzer.has_winner(grid)

        # then
        self.assertFalse(has_winner)

    def test_has_winner_should_return_true_if_winner_from_fourth_column_on_second_row(self):
        # given
        grid = GridFactory.with_winner_from_fourth_column_on_second_row()

        # when
        has_winner = self.analyzer.has_winner(grid)

        # then
        self.assertTrue(has_winner)

    def test_has_winner_should_return_true_if_winner_from_fourth_column_on_third_row(self):
        # given
        grid = GridFactory.with_winner_from_fourth_column_on_third_row()

        # when
        has_winner = self.analyzer.has_winner(grid)

        # then
        self.assertTrue(has_winner)

    def test_has_winner_should_return_true_if_winner_on_first_column(self):
        # given
        grid = GridFactory.with_winner_on_first_column_and_first_row()

        # when
        has_winner = self.analyzer.has_winner(grid)

        # then
        self.assertTrue(has_winner)

    def test_has_winner_should_return_true_if_winner_on_second_column(self):
        # given
        grid = GridFactory.with_winner_on_second_column_and_first_row()

        # when
        has_winner = self.analyzer.has_winner(grid)

        # then
        self.assertTrue(has_winner)

    def test_has_winner_should_return_true_if_winner_on_second_column_and_third_row(self):
        # given
        grid = GridFactory.with_winner_from_second_column_on_first_row()

        # when
        has_winner = self.analyzer.has_winner(grid)

        # then
        self.assertTrue(has_winner)

    def test_has_winner_should_return_false_if_no_winner_on_first_column(self):
        # given
        grid = GridFactory.without_winner_on_first_column()

        # when
        has_winner = self.analyzer.has_winner(grid)

        # then
        self.assertFalse(has_winner)

    def test_has_winner_should_return_true_if_winner_from_third_column_third_row(self):
        # given
        grid = GridFactory.with_winner_on_diagonal_up_right_from_third_column_third_row()

        # when
        has_winner = self.analyzer.has_winner(grid)

        # then
        self.assertTrue(has_winner)

    def test_has_winner_should_return_true_if_winner_from_first_column_first_row(self):
        # given
        grid = GridFactory.with_diagonal_up_right_from_first_column_first_row()

        # when
        has_winner = self.analyzer.has_winner(grid)

        # then
        self.assertTrue(has_winner)

    def test_has_winner_should_return_false_if_no_winner_in_diagonal(self):
        # given
        grid = GridFactory.without_winner_on_diagonal_up_right_from_first_column_first_row()

        # when
        has_winner = self.analyzer.has_winner(grid)

        # then
        self.assertTrue(has_winner)

    def test_is_draw_should_return_true_if_grid_is_full_with_no_winner(self):
        # given
        grid = GridFactory.full_without_winner()

        # when
        draw = self.analyzer.is_draw(grid)

        # then
        self.assertTrue(draw)

    def test_is_draw_should_return_false_if_grid_is_full_with_winner(self):
        # given
        grid = GridFactory.full_with_winner()

        # when
        draw = self.analyzer.is_draw(grid)

        # then
        self.assertFalse(draw)

    def test_is_draw_should_return_false_if_grid_is_not_full_with_winner(self):
        # given
        grid = GridFactory.with_winner_from_first_column_on_first_row()

        # when
        draw = self.analyzer.is_draw(grid)

        # then
        self.assertFalse(draw)


if __name__ == '__main__':
    unittest.main()
