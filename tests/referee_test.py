import unittest
from unittest.mock import Mock

from game.analyzer import Analyzer
from game.domain.coin import Coin
from game.domain.full_column_error import FullColumnError
from game.domain.game_state import GameState
from game.grid import Grid
from game.referee import Referee


class TestReferee(unittest.TestCase):
    def setUp(self):
        self.grid = Mock(Grid)
        self.analyzer = Mock(Analyzer)
        self.referee = Referee(grid=self.grid, analyzer=self.analyzer)
        self.grid.WIDTH = 7
        self.analyzer.is_draw.return_value = False
        self.analyzer.has_winner.return_value = False

    def test_play_should_return_the_current_player(self):
        # when
        game_state = self.referee.play(2)

        # then
        self.assertEqual(game_state, GameState.blue_turn)

    def test_play_should_alternate_the_current_player(self):
        # given
        self.referee.play(2)

        # when
        game_state = self.referee.play(2)

        # then
        self.assertEqual(game_state, GameState.green_turn)

    def test_play_should_push_a_green_coin_to_the_grid_at_given_column_minus_one_if_green_is_playing(self):
        # when
        self.referee.play(4)

        # then
        self.grid.push_coin.assert_called_once_with(3, Coin.green)

    def test_play_should_push_a_blue_coin_to_the_grid_at_given_column_minus_one_if_blue_is_playing(self):
        # given
        self.referee.play(4)

        # when
        self.referee.play(4)

        # then
        self.grid.push_coin.assert_called_with(3, Coin.blue)

    def test_play_should_not_alternate_the_current_player_if_column_is_full(self):
        # given
        initial_player = self.referee.get_next_player()
        self.grid.push_coin.side_effect = FullColumnError("Column 1 is full")
        self.referee.play(1)

        # when
        next_player = self.referee.get_next_player()

        # then
        self.assertEqual(next_player, initial_player)

    def test_play_should_not_alternate_the_current_player_if_column_is_lesser_than_one(self):
        # given
        initial_player = self.referee.get_next_player()

        # when
        next_player = self.referee.play(0)

        # then
        self.assertEqual(next_player, initial_player)

    def test_play_should_alternate_the_current_player_if_column_is_seven(self):
        # given
        initial_player = self.referee.get_next_player()

        # when
        next_player = self.referee.play(7)

        # then
        self.assertNotEqual(next_player, initial_player)

    def test_play_should_alternate_the_current_player_if_column_is_one(self):
        # given
        initial_player = self.referee.get_next_player()

        # when
        next_player = self.referee.play(1)

        # then
        self.assertNotEqual(next_player, initial_player)

    def test_play_should_not_alternate_the_current_player_if_column_is_greater_than_seven(self):
        # given
        initial_player = self.referee.get_next_player()

        # when
        next_player = self.referee.play(8)

        # then
        self.assertEqual(next_player, initial_player)

    def test_play_should_not_alternate_the_current_player_if_column_is_not_an_integer(self):
        # given
        initial_player = self.referee.get_next_player()

        # when
        next_player = self.referee.play("five")

        # then
        self.assertEqual(next_player, initial_player)

    def test_play_should_return_draw_if_the_game_is_draw(self):
        # given
        self.analyzer.is_draw.return_value = True

        # when
        game_state = self.referee.play(2)

        # then
        self.assertEqual(game_state, GameState.draw)

    def test_play_should_return_green_win_if_green_just_played_and_won(self):
        # given
        self.referee.set_next_player(GameState.green_turn)
        self.analyzer.has_winner.return_value = True

        # when
        game_state = self.referee.play(2)

        # then
        self.assertEqual(game_state, GameState.green_win)

    def test_play_should_return_blue_win_if_blue_just_played_and_won(self):
        # given
        self.referee.set_next_player(GameState.blue_turn)
        self.analyzer.has_winner.return_value = True

        # when
        game_state = self.referee.play(2)

        # then
        self.assertEqual(game_state, GameState.blue_win)

    def test_get_printed_grid_should_return_the_grid_pretty_print(self):
        # given
        self.grid.pretty_print.return_value = "some grid"

        # when
        printed_grid = self.referee.get_printed_grid()

        # then
        self.assertEqual(printed_grid, "some grid")


if __name__ == '__main__':
    unittest.main()
