import unittest
from unittest.mock import Mock

from game.domain.game_state import GameState
from game.game import Game


class TestGame(unittest.TestCase):
    def setUp(self):
        self.referee = Mock()
        self.console = Mock()
        self.game = Game(referee=self.referee, console=self.console)
        self.referee.get_next_player.return_value = GameState.blue_turn

    def test_start_should_print_a_welcome_message(self):
        # when
        self.game.start()

        # then
        self.console.print.assert_any_call("Hello ! Game is starting now...")

    def test_start_should_print_the_initial_game_state(self):
        # given
        self.referee.get_next_player.return_value = GameState.green_turn

        # when
        self.game.start()

        # then
        self.console.print.assert_any_call(GameState.green_turn.value)

    def test_start_should_print_the_initial_game_state(self):
        # given
        self.referee.get_printed_grid.return_value = "some grid"

        # when
        self.game.start()

        # then
        self.console.print.assert_any_call("some grid")

    def test_game_on_should_read_from_standard_input(self):
        # when
        self.game.game_on()

        # then
        self.console.read.assert_called_with("Your move ?")

    def test_game_on_should_play_with_column_read_from_console(self):
        # given
        self.console.read.return_value = "2"

        # when
        self.game.game_on()

        # then
        self.referee.play.assert_called_with("2")

    def test_game_on_should_print_game_state_after_turn(self):
        # given
        self.referee.play.return_value = GameState.green_turn

        # when
        self.game.game_on()

        # then
        self.console.print.assert_any_call(GameState.green_turn.value)

    def test_game_on_should_print_the_grid_after_turn(self):
        # given
        self.referee.get_printed_grid.return_value = "some grid"

        # when
        self.game.game_on()

        # then
        self.console.print.assert_any_call("some grid")

    def test_game_on_should_return_true_if_game_state_is_green_turn(self):
        # given
        self.referee.play.return_value = GameState.green_turn

        # when
        game_on = self.game.game_on()

        # then
        self.assertTrue(game_on)

    def test_game_on_should_return_true_if_game_state_is_a_blue_turn(self):
        # given
        self.referee.play.return_value = GameState.blue_turn

        # when
        game_on = self.game.game_on()

        # then
        self.assertTrue(game_on)

    def test_game_on_should_return_false_if_game_state_is_a_draw(self):
        # given
        self.referee.play.return_value = GameState.draw

        # when
        game_on = self.game.game_on()

        # then
        self.assertFalse(game_on)

    def test_game_on_should_return_false_if_game_state_is_a_blue_win(self):
        # given
        self.referee.play.return_value = GameState.blue_win

        # when
        game_on = self.game.game_on()

        # then
        self.assertFalse(game_on)

    def test_game_on_should_return_false_if_game_state_is_a_green_win(self):
        # given
        self.referee.play.return_value = GameState.green_win

        # when
        game_on = self.game.game_on()

        # then
        self.assertFalse(game_on)


if __name__ == '__main__':
    unittest.main()
