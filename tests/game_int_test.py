import unittest
from unittest.mock import Mock

from game.analyzer import Analyzer
from game.game import Game
from game.grid import Grid
from game.referee import Referee


class TestGameInt(unittest.TestCase):
    def test_game_on_should_use_instance_of_referee_to_make_the_game_happen(self):
        # given
        self.console = Mock()
        self.game = Game(console=self.console)
        self.console.read.return_value = "2"

        expected_print = "|. . . . . . .\n" \
                         "|. . . . . . .\n" \
                         "|. . . . . . .\n" \
                         "|. . . . . . .\n" \
                         "|. . . . . . .\n" \
                         "|. O . . . . .\n" \
                         " ^ ^ ^ ^ ^ ^ ^\n" \
                         " 1 2 3 4 5 6 7\n"

        # when
        game_on = self.game.game_on()

        # then
        self.console.print.assert_any_call(expected_print)
        self.assertTrue(game_on)


if __name__ == '__main__':
    unittest.main()
