import unittest

from game.domain.coin import Coin
from game.domain.full_column_error import FullColumnError
from game.grid import Grid
from tests.grid_factory import GridFactory


class TestGrid(unittest.TestCase):
    def setUp(self):
        self.grid = Grid()

    def test_cells_should_be_empty_after_instantiation(self):
        for column in range(7):
            for row in range(6):
                self.assertEqual(self.grid.get_cell_value(column, row), Coin.empty)

    def test_get_cell_value_should_return_the_coin_value(self):
        # given
        self.grid.push_coin(1, Coin.green)

        # when
        cell_value = self.grid.get_cell_value(1, 0)

        # then
        self.assertEqual(cell_value, Coin.green)

    def test_push_coin_should_put_a_coin_in_a_column(self):
        # when
        self.grid.push_coin(1, Coin.blue)

        # then
        self.assertEqual(self.grid.get_cell_value(1, 0), Coin.blue)
        self.assertEqual(self.grid.get_cell_value(1, 1), Coin.empty)

    def test_push_coin_should_stack_coins_in_a_column(self):
        # given
        self.grid.push_coin(1, Coin.green)

        # when
        self.grid.push_coin(1, Coin.blue)

        # then
        self.assertEqual(self.grid.get_cell_value(1, 1), Coin.blue)
        self.assertEqual(self.grid.get_cell_value(1, 2), Coin.empty)

    def test_push_coin_raise_an_exception_when_playing_in_a_full_column(self):
        # given
        for row in range(6):
            self.grid.push_coin(0, Coin.green)

        # then
        self.assertRaises(FullColumnError, self.grid.push_coin, 0, Coin.blue)

    def test_is_empty_should_return_true_if_grid_has_no_coin(self):
        self.assertTrue(self.grid.is_empty())

    def test_is_empty_should_return_false_if_grid_has_at_least_one_coin(self):
        # given
        self.grid.push_coin(1, Coin.green)

        # then
        self.assertFalse(self.grid.is_empty())

    def test_get_column_should_return_a_column_given_its_index(self):
        # given
        self.grid.push_coin(3, Coin.green)
        self.grid.push_coin(3, Coin.green)
        self.grid.push_coin(3, Coin.blue)

        # when
        column = self.grid.get_column(3)

        # then
        self.assertEqual(column, [Coin.green, Coin.green, Coin.blue, Coin.empty, Coin.empty, Coin.empty])

    def test_get_column_should_return_a_column_given_its_index(self):
        # given
        self.grid.push_coin(0, Coin.green)
        self.grid.push_coin(1, Coin.green)
        self.grid.push_coin(2, Coin.blue)

        self.grid.push_coin(0, Coin.blue)
        self.grid.push_coin(1, Coin.green)
        self.grid.push_coin(2, Coin.blue)

        # when
        row = self.grid.get_row(1)

        # then
        self.assertEqual(row, [Coin.blue, Coin.green, Coin.blue, Coin.empty, Coin.empty, Coin.empty, Coin.empty])

    def test_get_diagonal_from_cell_should_return_an_array_up_right_from_first_column_first_row(self):
        # given
        grid = GridFactory.with_diagonal_up_right_from_first_column_first_row()

        # when
        coins = grid.get_diagonal_from_cell(0, 0)

        # then
        self.assertEqual(coins, [Coin.green] * 4)

    def test_get_diagonal_from_cell_should_return_an_array_up_right_from_fourth_column_first_row(self):
        # given
        grid = GridFactory.with_diagonal_up_right_from_fourth_column_first_row()

        # when
        coins = grid.get_diagonal_from_cell(3, 0)

        # then
        self.assertEqual(coins, [Coin.green] * 4)

    def test_get_diagonal_from_cell_should_return_an_array_up_right_from_fourth_column_third_row(self):
        # given
        grid = GridFactory.with_diagonal_up_right_from_fourth_column_third_row()

        # when
        coins = grid.get_diagonal_from_cell(3, 2)

        # then
        self.assertEqual(coins, [Coin.green] * 4)

    def test_get_diagonal_from_cell_should_return_an_array_up_right_from_first_column_third_row(self):
        # given
        grid = GridFactory.with_diagonal_up_right_from_first_column_third_row()

        # when
        coins = grid.get_diagonal_from_cell(0, 2)

        # then
        self.assertEqual(coins, [Coin.green] * 4)

    def test_get_diagonal_from_cell_should_return_an_array_up_left_from_seventh_column_first_row(self):
        # given
        grid = GridFactory.with_diagonal_up_left_from_seventh_column_first_row()

        # when
        coins = grid.get_diagonal_from_cell(6, 0)

        # then
        self.assertEqual(coins, [Coin.green] * 4)

    def test_get_diagonal_from_cell_should_return_an_array_up_left_from_seventh_column_third_row(self):
        # given
        grid = GridFactory.with_diagonal_up_right_from_seventh_column_third_row()

        # when
        coins = grid.get_diagonal_from_cell(6, 2)

        # then
        self.assertEqual(coins, [Coin.green] * 4)

    def test_get_diagonal_from_cell_should_return_an_array_up_left_from_fifth_column_third_row(self):
        # given
        grid = GridFactory.with_diagonal_up_right_from_fifth_column_third_row()

        # when
        coins = grid.get_diagonal_from_cell(4, 2)

        # then
        self.assertEqual(coins, [Coin.green] * 4)

    def test_pretty_print_should_render_the_grid_with_coin_symbols(self):
        # given
        self.grid = Grid()
        self.grid.push_coin(1, Coin.green)
        self.grid.push_coin(1, Coin.blue)
        self.grid.push_coin(2, Coin.green)
        self.grid.push_coin(3, Coin.blue)

        expected_print = "|. . . . . . .\n" \
                         "|. . . . . . .\n" \
                         "|. . . . . . .\n" \
                         "|. . . . . . .\n" \
                         "|. X . . . . .\n" \
                         "|. O O X . . .\n" \
                         " ^ ^ ^ ^ ^ ^ ^\n" \
                         " 1 2 3 4 5 6 7\n"

        # when
        pretty_print = self.grid.pretty_print()

        # then
        self.assertEqual(pretty_print, expected_print)

    def test_is_full_should_return_false_if_there_are_empty_cells(self):
        # given
        self.grid.push_coin(1, Coin.green)
        self.grid.push_coin(5, Coin.blue)

        # when
        full = self.grid.is_full()

        # then
        self.assertFalse(full)

    def test_is_full_should_return_true_if_there_are_no_empty_cells_left(self):
        # given
        for row in reversed(range(self.grid.HEIGHT)):
            for column in range(self.grid.WIDTH):
                self.grid.push_coin(column, Coin.green)

        # when
        full = self.grid.is_full()

        # then
        self.assertTrue(full)


if __name__ == '__main__':
    unittest.main()
