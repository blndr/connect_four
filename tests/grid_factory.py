from game.domain.coin import Coin
from game.grid import Grid


class GridFactory:
    @staticmethod
    def without_winner_on_diagonal_up_right_from_first_column_first_row():
        grid = Grid()
        grid.push_coin(0, Coin.blue)
        grid.push_coin(1, Coin.blue)
        grid.push_coin(2, Coin.blue)
        grid.push_coin(3, Coin.blue)
        grid.push_coin(1, Coin.green)
        grid.push_coin(2, Coin.blue)
        grid.push_coin(2, Coin.green)
        grid.push_coin(3, Coin.blue)
        grid.push_coin(3, Coin.blue)
        grid.push_coin(3, Coin.green)
        return grid

    @staticmethod
    def with_diagonal_up_right_from_first_column_first_row():
        grid = Grid()
        grid.push_coin(0, Coin.green)
        grid.push_coin(1, Coin.blue)
        grid.push_coin(2, Coin.blue)
        grid.push_coin(3, Coin.blue)
        grid.push_coin(1, Coin.green)
        grid.push_coin(2, Coin.blue)
        grid.push_coin(2, Coin.green)
        grid.push_coin(3, Coin.blue)
        grid.push_coin(3, Coin.blue)
        grid.push_coin(3, Coin.green)
        return grid

    @staticmethod
    def with_diagonal_up_right_from_fourth_column_first_row():
        grid = Grid()
        grid.push_coin(3, Coin.green)
        grid.push_coin(4, Coin.blue)
        grid.push_coin(5, Coin.blue)
        grid.push_coin(6, Coin.blue)
        grid.push_coin(4, Coin.green)
        grid.push_coin(5, Coin.blue)
        grid.push_coin(5, Coin.green)
        grid.push_coin(6, Coin.blue)
        grid.push_coin(6, Coin.blue)
        grid.push_coin(6, Coin.green)
        return grid

    @staticmethod
    def with_diagonal_up_right_from_fourth_column_third_row():
        grid = Grid()
        grid.push_coin(3, Coin.green)
        grid.push_coin(3, Coin.blue)
        grid.push_coin(3, Coin.green)

        grid.push_coin(4, Coin.blue)
        grid.push_coin(4, Coin.blue)
        grid.push_coin(4, Coin.green)
        grid.push_coin(4, Coin.green)

        grid.push_coin(5, Coin.blue)
        grid.push_coin(5, Coin.blue)
        grid.push_coin(5, Coin.blue)
        grid.push_coin(5, Coin.blue)
        grid.push_coin(5, Coin.green)

        grid.push_coin(6, Coin.blue)
        grid.push_coin(6, Coin.blue)
        grid.push_coin(6, Coin.green)
        grid.push_coin(6, Coin.green)
        grid.push_coin(6, Coin.blue)
        grid.push_coin(6, Coin.green)
        return grid

    @staticmethod
    def with_diagonal_up_right_from_first_column_third_row():
        grid = Grid()
        grid.push_coin(0, Coin.green)
        grid.push_coin(0, Coin.blue)
        grid.push_coin(0, Coin.green)

        grid.push_coin(1, Coin.blue)
        grid.push_coin(1, Coin.blue)
        grid.push_coin(1, Coin.green)
        grid.push_coin(1, Coin.green)

        grid.push_coin(2, Coin.blue)
        grid.push_coin(2, Coin.blue)
        grid.push_coin(2, Coin.blue)
        grid.push_coin(2, Coin.blue)
        grid.push_coin(2, Coin.green)

        grid.push_coin(3, Coin.blue)
        grid.push_coin(3, Coin.blue)
        grid.push_coin(3, Coin.green)
        grid.push_coin(3, Coin.green)
        grid.push_coin(3, Coin.blue)
        grid.push_coin(3, Coin.green)
        return grid

    @staticmethod
    def with_diagonal_up_left_from_seventh_column_first_row():
        grid = Grid()
        grid.push_coin(6, Coin.green)

        grid.push_coin(5, Coin.green)
        grid.push_coin(5, Coin.green)

        grid.push_coin(4, Coin.blue)
        grid.push_coin(4, Coin.green)
        grid.push_coin(4, Coin.green)

        grid.push_coin(3, Coin.green)
        grid.push_coin(3, Coin.green)
        grid.push_coin(3, Coin.blue)
        grid.push_coin(3, Coin.green)
        return grid

    @staticmethod
    def with_diagonal_up_right_from_seventh_column_third_row():
        grid = Grid()
        grid.push_coin(3, Coin.green)
        grid.push_coin(3, Coin.blue)
        grid.push_coin(3, Coin.green)
        grid.push_coin(3, Coin.green)
        grid.push_coin(3, Coin.green)
        grid.push_coin(3, Coin.green)

        grid.push_coin(4, Coin.blue)
        grid.push_coin(4, Coin.blue)
        grid.push_coin(4, Coin.green)
        grid.push_coin(4, Coin.green)
        grid.push_coin(4, Coin.green)

        grid.push_coin(5, Coin.blue)
        grid.push_coin(5, Coin.blue)
        grid.push_coin(5, Coin.blue)
        grid.push_coin(5, Coin.green)

        grid.push_coin(6, Coin.green)
        grid.push_coin(6, Coin.blue)
        grid.push_coin(6, Coin.green)
        return grid

    @staticmethod
    def with_diagonal_up_right_from_fifth_column_third_row():
        grid = Grid()
        grid.push_coin(1, Coin.blue)
        grid.push_coin(1, Coin.blue)
        grid.push_coin(1, Coin.blue)
        grid.push_coin(1, Coin.blue)
        grid.push_coin(1, Coin.blue)
        grid.push_coin(1, Coin.green)

        grid.push_coin(2, Coin.blue)
        grid.push_coin(2, Coin.blue)
        grid.push_coin(2, Coin.blue)
        grid.push_coin(2, Coin.blue)
        grid.push_coin(2, Coin.green)

        grid.push_coin(3, Coin.blue)
        grid.push_coin(3, Coin.blue)
        grid.push_coin(3, Coin.blue)
        grid.push_coin(3, Coin.green)

        grid.push_coin(4, Coin.blue)
        grid.push_coin(4, Coin.blue)
        grid.push_coin(4, Coin.green)
        return grid

    @staticmethod
    def with_winner_on_diagonal_up_right_from_third_column_third_row():
        grid = Grid()
        grid.push_coin(5, Coin.blue)
        grid.push_coin(5, Coin.blue)
        grid.push_coin(5, Coin.green)
        grid.push_coin(5, Coin.green)
        grid.push_coin(5, Coin.blue)
        grid.push_coin(5, Coin.green)

        grid.push_coin(4, Coin.green)
        grid.push_coin(4, Coin.green)
        grid.push_coin(4, Coin.blue)
        grid.push_coin(4, Coin.blue)
        grid.push_coin(4, Coin.green)

        grid.push_coin(3, Coin.blue)
        grid.push_coin(3, Coin.blue)
        grid.push_coin(3, Coin.green)
        grid.push_coin(3, Coin.green)

        grid.push_coin(2, Coin.blue)
        grid.push_coin(2, Coin.blue)
        grid.push_coin(2, Coin.green)
        return grid

    @staticmethod
    def with_winner_from_first_column_on_first_row():
        grid = Grid()
        grid.push_coin(0, Coin.blue)
        grid.push_coin(1, Coin.blue)
        grid.push_coin(2, Coin.blue)
        grid.push_coin(3, Coin.blue)
        return grid

    @staticmethod
    def with_winner_from_second_column_on_first_row():
        grid = Grid()
        grid.push_coin(1, Coin.blue)
        grid.push_coin(2, Coin.blue)
        grid.push_coin(3, Coin.blue)
        grid.push_coin(4, Coin.blue)
        return grid

    @staticmethod
    def with_winner_from_third_column_on_first_row():
        grid = Grid()
        grid.push_coin(2, Coin.blue)
        grid.push_coin(3, Coin.blue)
        grid.push_coin(4, Coin.blue)
        grid.push_coin(5, Coin.blue)
        return grid

    @staticmethod
    def with_winner_from_fourth_column_on_first_row():
        grid = Grid()
        grid.push_coin(3, Coin.blue)
        grid.push_coin(4, Coin.blue)
        grid.push_coin(5, Coin.blue)
        grid.push_coin(6, Coin.blue)
        return grid

    @staticmethod
    def without_winner_on_first_row():
        grid = Grid()
        grid.push_coin(3, Coin.blue)
        grid.push_coin(4, Coin.green)
        grid.push_coin(5, Coin.green)
        grid.push_coin(6, Coin.blue)
        return grid

    @staticmethod
    def with_winner_from_fourth_column_on_second_row():
        grid = Grid()
        grid.push_coin(3, Coin.blue)
        grid.push_coin(4, Coin.green)
        grid.push_coin(5, Coin.green)
        grid.push_coin(6, Coin.blue)
        grid.push_coin(3, Coin.blue)
        grid.push_coin(4, Coin.blue)
        grid.push_coin(5, Coin.blue)
        grid.push_coin(6, Coin.blue)
        return grid

    @staticmethod
    def with_winner_from_fourth_column_on_third_row():
        grid = Grid()
        grid.push_coin(3, Coin.blue)
        grid.push_coin(4, Coin.green)
        grid.push_coin(5, Coin.green)
        grid.push_coin(6, Coin.blue)
        grid.push_coin(3, Coin.green)
        grid.push_coin(4, Coin.blue)
        grid.push_coin(5, Coin.green)
        grid.push_coin(6, Coin.blue)
        grid.push_coin(3, Coin.green)
        grid.push_coin(4, Coin.green)
        grid.push_coin(5, Coin.green)
        grid.push_coin(6, Coin.green)
        return grid

    @staticmethod
    def with_winner_on_first_column_and_first_row():
        grid = Grid()
        grid.push_coin(0, Coin.blue)
        grid.push_coin(0, Coin.blue)
        grid.push_coin(0, Coin.blue)
        grid.push_coin(0, Coin.blue)
        return grid

    @staticmethod
    def without_winner_on_first_column():
        grid = Grid()
        grid.push_coin(0, Coin.blue)
        grid.push_coin(0, Coin.green)
        grid.push_coin(0, Coin.blue)
        grid.push_coin(0, Coin.green)
        return grid

    @staticmethod
    def with_winner_on_second_column_and_first_row():
        grid = Grid()
        grid.push_coin(1, Coin.blue)
        grid.push_coin(1, Coin.blue)
        grid.push_coin(1, Coin.blue)
        grid.push_coin(1, Coin.blue)
        return grid

    @staticmethod
    def with_winner_on_second_column_and_thirds_row():
        grid = Grid()
        grid.push_coin(1, Coin.green)
        grid.push_coin(1, Coin.green)
        grid.push_coin(1, Coin.blue)
        grid.push_coin(1, Coin.blue)
        grid.push_coin(1, Coin.blue)
        grid.push_coin(1, Coin.blue)
        return grid

    @staticmethod
    def full_without_winner():
        grid = Grid()

        for column in [0, 2, 4, 6]:
            grid.push_coin(column, Coin.green)
            grid.push_coin(column, Coin.green)
            grid.push_coin(column, Coin.blue)
            grid.push_coin(column, Coin.blue)
            grid.push_coin(column, Coin.green)
            grid.push_coin(column, Coin.green)

        for column in [1, 3, 5]:
            grid.push_coin(column, Coin.blue)
            grid.push_coin(column, Coin.blue)
            grid.push_coin(column, Coin.green)
            grid.push_coin(column, Coin.green)
            grid.push_coin(column, Coin.blue)
            grid.push_coin(column, Coin.blue)

        return grid

    @staticmethod
    def full_with_winner():
        grid = Grid()

        for column in range(7):
            grid.push_coin(column, Coin.green)
            grid.push_coin(column, Coin.green)
            grid.push_coin(column, Coin.blue)
            grid.push_coin(column, Coin.blue)
            grid.push_coin(column, Coin.green)
            grid.push_coin(column, Coin.green)

        return grid
