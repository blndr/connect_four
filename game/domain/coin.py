from enum import Enum


class Coin(Enum):
    blue = 'X'
    green = 'O'
    empty = '.'
