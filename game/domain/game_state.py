from enum import Enum


class GameState(Enum):
    blue_turn = "BLUE player, your turn !"
    blue_win = "The BLUE player won the game \o/"
    green_turn = "GREEN player, your turn !"
    green_win = "The GREEN player won the game \o/"
    draw = "Draw :-("
