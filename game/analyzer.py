from game.domain.coin import Coin


class Analyzer:
    def __init__(self):
        self.MAX_START_HEIGHT_FOR_DIAGONALS = 3

    def is_draw(self, grid):
        return grid.is_full() and not self.__has_winner(grid)

    def has_winner(self, grid):
        return self.__has_winner(grid)

    def __has_winner(self, grid):
        return self.__winner_on_rows(grid) or self.__winner_on_columns(grid) or self.__winner_on_diagonals(grid)

    def __winner_on_diagonals(self, grid):
        for column in range(grid.WIDTH):
            for row in range(self.MAX_START_HEIGHT_FOR_DIAGONALS):
                coins = grid.get_diagonal_from_cell(column, row)
                if self.__same_colors(coins):
                    return True

    def __winner_on_columns(self, grid):
        for column in range(grid.WIDTH):
            if self.__has_winner_in_set(grid, grid.get_column(column)):
                return True

    def __winner_on_rows(self, grid):
        for row in range(grid.HEIGHT):
            if self.__has_winner_in_set(grid, grid.get_row(row)):
                return True

    def __has_winner_in_set(self, grid, coins):
        for start in range(grid.WINNING_LENGTH):
            if self.__same_colors(coins[start:start + grid.WINNING_LENGTH]):
                return True

    @staticmethod
    def __same_colors(coins):
        return coins.count(Coin.green) == len(coins) or coins.count(Coin.blue) == len(coins)
