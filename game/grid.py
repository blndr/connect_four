from game.domain.coin import Coin
from game.domain.full_column_error import FullColumnError


class Grid:
    def __init__(self):
        self.WIDTH = 7
        self.HEIGHT = 6
        self.WINNING_LENGTH = 4
        self.__cells = [[Coin.empty] * self.HEIGHT for column in range(self.WIDTH)]

    def push_coin(self, column, coin):
        current_column = self.__cells[column]
        try:
            empty_row = self.__find_first_empty_cell(current_column)
            current_column[empty_row] = coin
        except StopIteration:
            raise FullColumnError("Column " + str(column) + " is full")

    def get_cell_value(self, column, row):
        return self.__cells[column][row]

    def pretty_print(self):
        pretty = ''
        for row in reversed(range(self.HEIGHT)):
            coins = (self.__cells[column][row].value for column in range(self.WIDTH))
            pretty += '|' + ' '.join(coins) + '\n'

        pretty += ' ^' * 7 + '\n'
        for i in range(1, 8):
            pretty += ' ' + str(i)

        return pretty + '\n'

    def is_empty(self):
        return self.__count_empty_cells() == self.WIDTH * self.HEIGHT

    def is_full(self):
        return self.__count_empty_cells() == 0

    def get_column(self, index):
        return self.__cells[index]

    def get_row(self, index):
        return [self.__cells[column][index] for column in range(self.WIDTH)]

    def get_diagonal_from_cell(self, column, row):
        way = -1 if column > 3 else 1
        return [self.get_cell_value(column + way * shift, row + shift) for shift in range(self.WINNING_LENGTH)]

    def __count_empty_cells(self):
        return sum(self.__cells[column].count(Coin.empty) for column in range(self.WIDTH))

    def __find_first_empty_cell(self, column):
        return next(row for row in range(self.HEIGHT) if column[row] == Coin.empty)
