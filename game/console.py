class Console:
    @staticmethod
    def print(message):
        print(message)

    @staticmethod
    def read(prompt):
        return input(prompt)

    @staticmethod
    def clear():
        print("\n" * 100)
