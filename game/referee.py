from wyre import inject

from game.analyzer import Analyzer
from game.domain.coin import Coin
from game.domain.full_column_error import FullColumnError
from game.domain.game_state import GameState
from game.grid import Grid


class Referee:
    @inject
    def __init__(self, grid=Grid, analyzer=Analyzer):
        self.__grid = grid
        self.__analyzer = analyzer
        self.__state = GameState.green_turn

    def play(self, column):
        if self.__valid_column(column):
            try:
                zero_indexed_column = int(column) - 1
                self.__grid.push_coin(zero_indexed_column, self.__coin_to_play())
                self.__update_state()
            except FullColumnError as error:
                print(error.message)

        return self.__state

    def get_printed_grid(self):
        return self.__grid.pretty_print()

    def get_next_player(self):
        return self.__state

    def set_next_player(self, next_player):
        self.__state = next_player

    def __update_state(self):
        if self.__analyzer.is_draw(self.__grid):
            self.__state = GameState.draw
        elif self.__analyzer.has_winner(self.__grid):
            self.__state = self.__winner()
        else:
            self.__state = self.__alternate_player()

    def __winner(self):
        return GameState.green_win if self.__state == GameState.green_turn else GameState.blue_win

    def __coin_to_play(self):
        return Coin.green if self.__state == GameState.green_turn else Coin.blue

    def __alternate_player(self):
        return GameState.green_turn if self.__state == GameState.blue_turn else GameState.blue_turn

    def __valid_column(self, column):
        try:
            return 0 < int(column) <= self.__grid.WIDTH
        except ValueError:
            return False
