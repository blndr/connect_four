from wyre import inject

from game.console import Console
from game.domain.game_state import GameState
from game.referee import Referee


class Game:
    @inject
    def __init__(self, referee=Referee, console=Console):
        self.__referee = referee
        self.__console = console

    def start(self):
        state = self.__referee.get_next_player()
        self.__console.print("Hello ! Game is starting now...")
        self.__console.print(self.__referee.get_printed_grid())
        self.__console.print(state.value)

    def game_on(self):
        column = self.__console.read("Your move ?")
        self.__console.clear()
        state = self.__referee.play(column)
        self.__console.print(self.__referee.get_printed_grid())
        self.__console.print(state.value)
        return state in [GameState.green_turn, GameState.blue_turn]
