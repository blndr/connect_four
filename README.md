# connect_four
A small "connect_four" CLI game implemented in Python, as a mean for me to try and learn the language !

## Run the game
To play a game, just do :
```
python3 run.py
```

### Python version
This software is not compatible with Python 2.x, you will have to install some version of python3 to get it running.

## Run the tests
To run the tests, install py.test from the requirements.txt file and use it :
```
pip install -r requirements.txt
py.test tests/
```
## Test coverage
`pytest-cov` will be installed using the `requirements.txt`.
To print out a test coverage report, type :
```
py.test --cov=game tests
```
